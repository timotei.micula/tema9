<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class GuessNumberController extends AbstractController
{
    /**
     * @Route("/guessNr", name="guess")
     */
    public function guessNr()
    {   
        $session = new Session();
        $session->start();
        $sessionNumber = $session->get('sessionNr');
        if(!isset($sessionNr)){
            $sessionNr = random_int(0, 100);
            $session->set('sessionNr',$sessionNr);
        }
        
        return $this->render('guessNumber.html.twig', [
            'guessNumber' => $sessionNr,
        ]);
    }
     
}

